package com.example.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intentName = getIntent();
        String name = intentName.getStringExtra("NameAnimal");
        final Animal animal = AnimalList.getAnimal(name);
        if(name != null){
            TextView nameAnimal = findViewById(R.id.animalName);
            nameAnimal.setText(name);

            ImageView avatar = findViewById(R.id.imageAnimal);
            avatar.setImageResource(getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName()));

            TextView esperanceVie = findViewById(R.id.animalHightestLifespan);
            esperanceVie.setText(animal.getStrHightestLifespan());

            TextView gestation = findViewById(R.id.animalGestatioinPeriod);
            gestation.setText(animal.getStrGestationPeriod());

            TextView weightBirth = findViewById(R.id.animalBirthWeight);
            weightBirth.setText(animal.getStrBirthWeight());

            TextView weightAdult = findViewById(R.id.animalAdultWeight);
            weightAdult.setText(animal.getStrAdultWeight());

            final TextView conservation = findViewById(R.id.conservationInfo);
            conservation.setText(animal.getConservationStatus());

            Button saveButton = findViewById(R.id.save);
            assert saveButton != null;

            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animal.setConservationStatus(conservation.getText().toString());
                    Toast.makeText(AnimalInfoActivity.this, "La sauvegarde a réussi", Toast.LENGTH_LONG).show();
                }
            });
        }

    }
}
