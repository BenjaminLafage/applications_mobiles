package com.example.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity extends AppCompatActivity {

    private final String[] animals = AnimalList.getNameArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_main);
        final ListView viewList = (ListView) findViewById(R.id.zooListView);*/
        setContentView(R.layout.activity_main);
        RecyclerView rv = findViewById(R.id.animalRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());

        /*final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animals);
        viewList.setAdapter(adapter);

        viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(MainActivity.this, AnimalInfoActivity.class);
                intent.putExtra("extraAnimalName",animals[position]);
                startActivity(intent);
            }
        });*/
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return new RowHolder(getLayoutInflater().inflate(R.layout.animal_recyclerview,parent,false));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position){
            Animal animal = AnimalList.getAnimal((animals[position]));
            int id = getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName());
            holder.bindModel(animals[position],id);
        }

        @Override
        public int getItemCount() {
            return animals.length;
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView iconAnimal = null;
        TextView animalNameView = null;
        RowHolder(View row) {
            super(row);
            iconAnimal = row.findViewById(R.id.iconAnimal);
            animalNameView = row.findViewById(R.id.animalNameView);
            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final String item = animalNameView.getText().toString();
            Intent intent = new Intent(MainActivity.this, AnimalInfoActivity.class);
            intent.putExtra("NameAnimal", item);
            startActivity(intent);
        }

        void bindModel(String name, int id) {
            animalNameView.setText(name);
            iconAnimal.setImageResource(id);
        }
    }
}
